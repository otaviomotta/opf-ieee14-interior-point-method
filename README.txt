Autor: Otávio dos Santos Motta

PT-BR

Para simular o algoritmo no MATLAB você deve primeiro fazer o download do arquivo "FPO_Linear.m", abrir ele dentro do MATLAB e depois clicar em "Run" (F5)

ENG

To simulate the algorithm in MATLAB you must first download the file "FPO_Linear.m", open it inside MATLAB and then click on "Run" (F5)