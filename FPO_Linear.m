%=============ATIVAÇÃO DO MÉTODO DE OTIMIZAÇÃO FMINCON=====================
clear all;
options = optimset('Algorithm','interior-point' );

%========================DADOS DE ENTRADA==================================
REFERENCIA=1; % Barra referência do sistema

% Dados de linha [De Para X]
DADOS_LINHA=[ 	1 2 0.0592;
                1 5 0.2230;
                2 3 0.1980;
                2 4 0.1763;
                2 5 0.1739;
                3 4 0.1710;
                4 5 0.0421;
                4 7 0.2091;
                4 9 0.5562;
                5 6 0.2520;
                6 11 0.1989;
                6 12 0.2558;
                6 13 0.1303;
                7 8 0.1762;
                7 9 0.1100;
                9 10 0.0845;
                9 14 0.2704;
                10 11 0.1921;
                12 13 0.1999;
                13 14 0.3480];
            
% Dados de Barra [BARRA PINJ TETA]     %Demanda de 268,29 MW [Sendo 259 MW de carga e 9,287 ~ 9,29 MW de perdas]
DADOS_BARRAS=[  1 (2.1307-0) 0;        %Pg1 Inserido após cálculo FPO Linear Pg1=2.1307pu Pd=0pu
                2 (0.5522-0.2170) 0;   %Pg2 Inserido após cálculo FPO Linear Pg2=0.5522pu Pd=0.2170pu
                3 -0.9420 0;
                4 -0.4780 0;
                5 -0.0760 0;
                6 -0.1120 0;
                7 -0.0929 0;
                8 0 0;
                9 -0.2950 0;
                10 -0.0900 0;
                11 -0.0350 0;
                12 -0.0610 0;
                13 -0.1350 0;
                14 -0.1490 0];

Ag =[1 0 0; 0 1 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0;]; %Matriz de alocação dos geradores.
PL(:,1) = [0; 0.2170; 0.9420; 0.4780; 0.0760; 0.1120; 0.0929; 0; 0.2950; 0.0900; 0.0350; 0.0610; 0.1350; 0.1490;]; %Vetor de Cargas

%==========================VALORES DE INICIAÇÃO============================
P(:,1) = [ 1.0654; 0.70;0]; % media entre Pmax e Pmin dos 2 Geradores
X0 = [0;0;0;0;0;0;0;0;0;0;0;0;0;P];

%======================LEITURA DOS DADOS DE ENTRADA========================
[NCOMPONENTES,NCOL]=size(DADOS_LINHA);               % Número de componentes – Linhas de transmissão
NBARRAS=max([DADOS_LINHA(:,1)'	DADOS_LINHA(:,2)']); % Número total de barras 

for i=1:NCOMPONENTES	
    DE(i)=DADOS_LINHA(i,1);	    % Barra origem
    PARA(i)=DADOS_LINHA(i,2);	% Barra destino
    X(i)=DADOS_LINHA(i,3);		% Reatância
    B(i)=1/X(i); 		        % Susceptância 
end

for i=1:NBARRAS
	PINJ(i)=DADOS_BARRAS(i,2);	% Potência ativa injetada
	TETA(i)=DADOS_BARRAS(i,3);	% Ângulos de potência
end

%===================MONTAGEM DA MATRIZ B_LINHA E BRED======================
B_LINHA=zeros(NBARRAS,NBARRAS);

for i=1:NCOMPONENTES
    K=DE(i);
    M=PARA(i);

    B_LINHA(K,K)=B_LINHA(K,K)+B(i); 	% Termo da diagonal
    B_LINHA(M,M)=B_LINHA(M,M)+B(i);  	% Termo da diagonal
    B_LINHA(K,M)=B_LINHA(K,M)-B(i); 	% Termos fora da diagonal
    B_LINHA(M,K)=B_LINHA(M,K)-B(i); 	% Termos fora da diagonal   
end 

B_RED = B_LINHA(:,2:14); % Bred representa B Reduzida, de modo que é desconsiderado a primeira linha e coluna, logo ",2:14"

%===========================EQUAÇÕES DE IGUALDADE==========================
Aeq=[B_RED (-Ag)];
beq=[-PL(:,1)];

%==========LIMITES INFERIORES E SUPERIORES DAS VARIÁVEIS===================
theta_UP = 100.*ones(13,1); theta_DOWN = -100*ones(13,1); % limite thetas=0
P_Up = [2.1307; 1.4]; P_Down(:,1) = [0; 0];
LB = [theta_DOWN; P_Down(:,1)];                           %Lower bounds Vector
UB = [theta_UP; P_Up];                                    %Upper bounds vector

%=============================OTIMIZAÇÃO===================================
[X,FVAL,EXITFLAG,OUTPUT,LAMBDA]=fmincon(@objfun,X0,[],[],Aeq,beq,LB,UB);

jx = 0;
for i = 1:13
jx = jx + 1;
theta1(i,1)= X(jx,1);
end

for i =1:2
jx = jx + 1;
pg(i,1) = X(jx,1);
end

theta(1,1) = 0.0;
jx = 1;
for i = 1:13
theta(jx+i,1) = theta1(i,1);
end

LambdaEQ = LAMBDA.eqlin;
jx = 0;
for i = 1:14
jx = jx + 1;
Lambda(i,1) = LambdaEQ(jx)./100;
end

%==================FLUXO DE POTÊNCIA ÓTIMO LINEARIZADO=====================
B_REF=REFERENCIA;	            % Barra referência do sistema
B_LINHA(B_REF,B_REF)=(10^50);	% Retirada da equação referente a barra de referência do sistema
TETA=(inv(B_LINHA))*(PINJ');

for i=1:NCOMPONENTES % Cálculo do fluxo de potência
	K=DE(i);
	M=PARA(i);
    FLUXO(i)= (B(i)*(TETA(K)-TETA(M)));   
end

%============================VARIÁVEIS DE SAÍDA============================
theta_rad = theta;
theta_graus = ((180/pi).*theta);
P=[pg(1,:);pg(2,:)];                             %Vetor com valores das potências geradas pelas unidades em pu
CPG1=(20*(100*P(1,1))+0.0430293*(100*P(1,1))^2); %Custo de produção unidade de geração 1
CPG2=(20*(100*P(2,1))+0.25*(100*P(2,1))^2);      %Custo de produção unidade de geração 2
CT=CPG1+CPG2;                                    %Custo Total com as duas unidades
CIPG1=(20+17.2*(100*P(1,1)));                    %Custo incremental unidade de geração 1
CIPG2=(20+21*(100*P(2,1)));                      %Custo incremental unidade de geração 2
PT=(100*P(1,1) + 100*P(2,1));

%================================DISPLAY===================================
format longG;

disp('    Pg1(MW)           Pg2(MW)           PgTotal(MW)');
fprintf('%12.4f     %12.4f       %12.4f \n', 100*P(1,1), 100*P(2,1), PT);
fprintf('\n');

disp('   Custo Usina 01($/h)   Custo Usina 02($/h)')
fprintf('%12.4f          %12.4f\n', CPG1, CPG2); 
fprintf('\n');

disp('   Custo Total($/h)')
fprintf('%12.4f\n', CT); 
fprintf('\n');

disp('   Custo Incremental Usina 01($/h)      Custo Incremental Usina 02($/h)')
fprintf('%12.4f                         %12.4f\n', CIPG1, CIPG2); 
fprintf('\n'); 

disp('   Lambda - Custo Marginal($/MWh)')
fprintf('%12.4f\n', Lambda(1,1)); 
fprintf('\n');

disp('   Ângulo de Fase(rad) pela Fmincon')
fprintf('%12.4f\n', [theta_rad]); 
fprintf('\n');
disp('   Ângulo de Fase(rad) pelo Fluxo de Potência Linear')
fprintf('%12.4f\n', [TETA]); 
fprintf('\n');

disp('   Fluxo de Potência Ativa(MW)')
fprintf('%12.4f\n', [100*FLUXO]); 
fprintf('\n');

disp('   Fluxo de Potência Ativa(pu)')
fprintf('%12.4f\n', [FLUXO]); 
fprintf('\n');

%============================FUNÇÃO OBJETIVO=============================== 
function f = objfun(X,co,b,Q);
co=[0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0];
b= [0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 20; 20];
Q= zeros(16,16);
Q(15,15)= 0.0430293;
Q(16,16)= 0.25;
f = (ones(1,16)*co)+(b'*X) + (0.5*X'*Q*X);
end
